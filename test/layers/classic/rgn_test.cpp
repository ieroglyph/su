#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators_all.hpp>

#include "tools/test_tools.hpp"
#include "rgn.hpp"

using su::Board;
using su::classic::Rgn;

using namespace sut;

SCENARIO("Creating a rgn", "[layers][classic][rgn]")
{
    const static auto boardstr{
        "1* 2* 13 14 15 16 17 18 19 "
        "12 2* 3* 24 25 26 27 28 29 "
        "13 23 3* 4* 35 36 37 38 39 "
        "14 24 34 4* 5* 46 47 48 49 "
        "15 25 35 45 5* 6* 57 58 59 "
        "16 26 36 46 56 6* 7* 68 69 "
        "17 27 37 47 57 67 7* 8* 79 "
        "18 28 38 48 58 68 78 8* 9* "
        "1* 29 39 49 59 69 79 89 9* " };

    const static auto boardstr2{
        "1* 2* 13 14 15 16 17 18 19 "
        "12 2* 3* 24 25 26 27 28 29 "
        "13 23 3* 4* 35 36 37 38 39 "
        "14 24 34 7* 7* 7* 47 48 49 "
        "15 25 35 7* 7* 7* 57 58 59 "
        "16 26 36 7* 7* 7* 7* 68 69 "
        "17 27 37 47 57 67 7* 8* 79 "
        "18 28 38 48 58 68 78 8* 9* "
        "1* 29 39 49 59 69 79 89 9* " };

    GIVEN("A given board")
    {
        Board b{ Formatter::parseBoard<D>(boardstr) };
        size_t idx = GENERATE(0, 8);
        THEN("We get full data for each rgn")
        {
            Rgn rgn(b, idx);
            REQUIRE(rgn.const_size == static_cast<size_t>(D));
            // TODO: cannot get size of filter | take, need to do something
            // size_t s(std::distance(rgn.digits().begin(), rgn.digits().end()));
            size_t s{ 0 };
            for (auto d : rgn.digits()) {
                s++;
            }
            INFO("rgn idx == " << static_cast<size_t>(idx));
            REQUIRE(s == static_cast<size_t>(D));
        }

        AND_THEN("Rgns are of correct set of ditigs")
        {
            Rgn rgn(b, 6);
            static_assert(range_of_printables<decltype(rgn.digits())>);
            auto repr{ Formatter::toString(rgn.digits()) };
            REQUIRE(repr == "17 27 37 18 28 38 1* 29 39 ");
        }

        AND_THEN("Digits are accessible through rgns")
        {
            Rgn rgn(b, 4);
            std::ranges::for_each(rgn.digits(), [](auto& d) { d.solve(7);});
            REQUIRE(Formatter::toString(b) == boardstr2);

        }
    }
}