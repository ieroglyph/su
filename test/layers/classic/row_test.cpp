#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators_all.hpp>

#include "tools/test_tools.hpp"
#include "row.hpp"

using su::Board;
using su::classic::Row;

using namespace sut;

SCENARIO("Creating a row", "[layers][classic][row]")
{
    const static auto boardstr{
        "1* 2* 13 14 15 16 17 18 19 "
        "12 2* 3* 24 25 26 27 28 29 "
        "13 23 3* 4* 35 36 37 38 39 "
        "14 24 34 4* 5* 46 47 48 49 "
        "15 25 35 45 5* 6* 57 58 59 "
        "16 26 36 46 56 6* 7* 68 69 "
        "17 27 37 47 57 67 7* 8* 79 "
        "18 28 38 48 58 68 78 8* 9* "
        "1* 29 39 49 59 69 79 89 9* " };

    const static auto boardstr2{
        "1* 2* 13 14 15 16 17 18 19 "
        "12 2* 3* 24 25 26 27 28 29 "
        "3* 3* 3* 3* 3* 3* 3* 3* 3* "
        "14 24 34 4* 5* 46 47 48 49 "
        "15 25 35 45 5* 6* 57 58 59 "
        "16 26 36 46 56 6* 7* 68 69 "
        "17 27 37 47 57 67 7* 8* 79 "
        "18 28 38 48 58 68 78 8* 9* "
        "1* 29 39 49 59 69 79 89 9* " };

    GIVEN("A given board")
    {
        Board b{ Formatter::parseBoard<D>(boardstr) };
        size_t idx = GENERATE(0, 8);
        THEN("We get full data for each row")
        {
            Row row(b, idx);
            REQUIRE(row.const_size == static_cast<size_t>(D));
            size_t s(std::distance(row.digits().begin(), row.digits().end()));
            INFO("row idx == " << static_cast<size_t>(idx));
            REQUIRE(s == static_cast<size_t>(D));
        }

        AND_THEN("Rows are of correct set of ditigs")
        {
            Row row(b, 3);
            static_assert(range_of_printables<decltype(row.digits())>);
            auto repr{ Formatter::toString(row.digits()) };
            REQUIRE(repr == "14 24 34 4* 5* 46 47 48 49 ");
        }

        AND_THEN("Digits are accessible through rows")
        {
            Row row(b, 2);
            std::ranges::for_each(row.digits(), [](auto& d) { d.solve(3);});
            REQUIRE(Formatter::toString(b) == boardstr2);

        }
    }
}