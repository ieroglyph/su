#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators_all.hpp>

#include "tools/test_tools.hpp"
#include "col.hpp"

using su::Board;
using su::classic::Col;

using namespace sut;

SCENARIO("Creating a col", "[layers][classic][col]")
{
    const static auto boardstr{
        "1* 2* 13 14 15 16 17 18 19 "
        "12 2* 3* 24 25 26 27 28 29 "
        "13 23 3* 4* 35 36 37 38 39 "
        "14 24 34 4* 5* 46 47 48 49 "
        "15 25 35 45 5* 6* 57 58 59 "
        "16 26 36 46 56 6* 7* 68 69 "
        "17 27 37 47 57 67 7* 8* 79 "
        "18 28 38 48 58 68 78 8* 9* "
        "1* 29 39 49 59 69 79 89 9* " };

    const static auto boardstr2{
        "1* 2* 13 14 5* 16 17 18 19 "
        "12 2* 3* 24 5* 26 27 28 29 "
        "13 23 3* 4* 5* 36 37 38 39 "
        "14 24 34 4* 5* 46 47 48 49 "
        "15 25 35 45 5* 6* 57 58 59 "
        "16 26 36 46 5* 6* 7* 68 69 "
        "17 27 37 47 5* 67 7* 8* 79 "
        "18 28 38 48 5* 68 78 8* 9* "
        "1* 29 39 49 5* 69 79 89 9* " };

    GIVEN("A given board")
    {
        Board b{ Formatter::parseBoard<D>(boardstr) };
        size_t idx = GENERATE(0, 8);
        THEN("We get full data for each col")
        {
            Col col(b, idx);
            REQUIRE(col.const_size == static_cast<size_t>(D));
            size_t s(std::distance(col.digits().begin(), col.digits().end()));
            INFO("col idx == " << static_cast<size_t>(idx));
            REQUIRE(s == static_cast<size_t>(D));
        }

        AND_THEN("Cols are of correct set of ditigs")
        {
            Col col(b, 6);
            static_assert(range_of_printables<decltype(col.digits())>);
            auto repr{ Formatter::toString(col.digits()) };
            REQUIRE(repr == "17 27 37 47 57 7* 7* 78 79 ");
        }

        AND_THEN("Digits are accessible through cols")
        {
            Col col(b, 4);
            std::ranges::for_each(col.digits(), [](auto& d) { d.solve(5);});
            REQUIRE(Formatter::toString(b) == boardstr2);

        }
    }
}