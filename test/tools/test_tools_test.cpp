#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators_all.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include "test_tools.hpp"
#include <vector>
#include <sstream>

using namespace sut;

namespace Catch {
template<std::ranges::range RangeT>
struct StringMaker<RangeT>
{
    static std::string convert(RangeT value)
    {
        std::ostringstream str;
        str << "{";
        size_t s(std::ranges::distance(value.begin(), value.end()));
        for (auto& v : value) {
            str << " " << digitToChar(v);
            if (--s) str << ',';
        }
        str << " }";
        return str.str();
    }
};
}

template<std::ranges::range RangeT>
struct EqualsRangeMatcher : Catch::Matchers::MatcherGenericBase
{
    EqualsRangeMatcher(RangeT const& range) :
        range{ range }
    {}

    bool match(auto&& other) const
    {
        return std::ranges::equal(range, other);
    }

    std::string describe() const override
    {
        return "Equals: " + Catch::rangeToString(range);
    }

private:
    RangeT range;
};

template<std::ranges::range RangeT>
auto EqualsRange(const RangeT& range) -> EqualsRangeMatcher<RangeT>
{
    return EqualsRangeMatcher<RangeT>{range};
};

SCENARIO("Formatting a digit", "[test]")
{
    GIVEN("A new digit")
    {
        Digit d;
        THEN("Formatting is correct")
        {
            REQUIRE(Formatter::toString(d) == fullDigitPrinted.data());
        }

        WHEN("Unmarking some of the values")
        {
            d.unmark(1);
            d.unmark(3);
            d.unmark(9);

            THEN("Formatting is correct")
            {
                REQUIRE(Formatter::toString(d) == "245678");
            }
        }

        WHEN("Solving a digit")
        {
            d.solve(1);

            THEN("Formatting is correct")
            {
                REQUIRE(Formatter::toString(d) == "1*");
            }
        }
    }
}

SCENARIO("Parsing a board", "[test]")
{
    GIVEN("Incorrectly formed board string")
    {
        const auto boardstr{ "1 2 3 4 5 6 6 6 6* 9 * 9 7 6 ? wtf" };

        WHEN("Attempting to parse it")
        {
            THEN("Throws an exception")
            {
                REQUIRE_THROWS(sut::Formatter::parseBoard<D>(boardstr));
            }
        }
    }

    GIVEN("Correctly formed board string")
    {
        const auto boardstr{ "1 2 3 4 5 6 7 8 9 " // 0-8
        "1* 2* 3* 4* 5* 6* 7* 8* 9* " // 9-17
        "12 23 34 45 56 67 78 89 91 " // 18-26
        "13 24 35 46 57 68 79 81 92 " // 27-35
        "123 234 345 456 567 678 789 891 912 " // 36-44
        "11 22 33 44 55 66 77 88 99 " // 45-53
        "17 27 37 47 57 67 77 87 97 " // 54-62
        "123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 " // 63-71
        "987 876 765 654 543 432 321 219 198" // 72-80
        };

        WHEN("Attempting to parse it")
        {
            Board b{ sut::Formatter::parseBoard<D>(boardstr) };

            THEN("Random checks show correct values")
            {
                REQUIRE(b.getDigit(0).getMarked().front() == 1);
                REQUIRE(b.getDigit(11).isSolved());
                REQUIRE(b.getDigit(11).solution().value() == 3);
                REQUIRE_THAT(b.getDigit(20).getMarked(), EqualsRange(std::array{ 3, 4 }));
                REQUIRE_THAT(b.getDigit(63).getMarked(), EqualsRange(std::array{ 1, 2, 3, 4, 5, 6, 7, 8, 9 }));
                REQUIRE_THAT(b.getDigit(80).getMarked(), EqualsRange(std::array{ 1, 8, 9 }));
            }
        }
    }
}