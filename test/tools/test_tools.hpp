#ifndef SU_TEST_TOOLS_HPP
#define SU_TEST_TOOLS_HPP

#include <algorithm>
#include <concepts>
#include <ranges>
#include <sstream>
#include <string>
#include <string_view>
#include <map>
#include <functional>

#include "printing.hpp"
#include "board.hpp"
#include "row.hpp"

using namespace su;
using std::ostream;
using std::views::filter;

namespace sut {

// forward declaring operators
template <digit_t MaxDigit = D>
inline static ostream& operator<<(ostream& s, const Digit<MaxDigit>& d);

template <digit_t MaxDigit = D, size_t NumberOfDigits = (D* D)>
inline static ostream& operator<<(ostream& s, Board<MaxDigit, NumberOfDigits>& b);

// concept for something that can be printed to osrteam
template <class T>
concept printable = requires(ostream & s, const T a)
{ s << a; };

// concept for a range of something that can be printed to ostream
template<class T>
concept range_of_printables = std::ranges::range<T> && requires (T t) { {*t.begin()} -> printable; };

/**
 * @brief Writes a range of printables
 * Each are separated by spaces
 * @param s Output string stream
 * @param digits Range of digits
 * @return ostringstream&
 */
inline static ostream& operator<<(ostream& s, range_of_printables auto& range)
{
    // std::ranges::copy(range | std::ranges::join_with_view(' '), std::ostream_iterator(s));
    std::ranges::for_each(range, [&s](auto r) { s << r << " ";});
    return s;
}

// obvious checks
static_assert(printable<int>);
static_assert(range_of_printables<array<int, 17>>);
// interesting one, would have been funny to have it working
// static_assert(printable<array<int, 17>>);

/**
* @brief Write digit state as plain text
* For unsolved digits, only the marked values will be printed, without spaces between them
* For solved digits, only the solution will be printed, marked with * after the value
* @param s Output string stream
* @param d Digit to write
* @return ostringstream&
*/
template <digit_t MaxDigit>
inline static ostream& operator<<(ostream& s, const Digit<MaxDigit>& d)
{
    if (d.isSolved()) {
        s << digitToChar(*d.solution()) << "*";
    } else {
        std::ranges::copy(d.getMarked() | d2c, std::ostream_iterator<char>(s));
    }
    return s;
}
static_assert(printable<Digit<D>>);

/**
* @brief Write board state as plain text
* Digits are separated by spaces, no additional separation for rows
* @param s Output string stream
* @param b Board to write
* @return ostringstream&
*/
template <digit_t MaxDigit, size_t NumberOfDigits>
inline static ostream& operator<<(ostream& s, Board<MaxDigit, NumberOfDigits>& b)
{
    s << b.allDigits();
    return s;
}

/**
* @brief Utils for printing and reading things
*/
class Formatter
{
public:
    /** @brief Prints something printable to string
     * Nice place to play with libfmt in future...
     */
    template<class T> requires printable<T> || range_of_printables<T>
    static std::string toString(T && d)
    {
        std::ostringstream s;
        s << d;
        return s.str();
    }

    /** @brief Reads board from string
     * Format is cursed as hell, and is supposed to be used in tests only.
     * That means that we can afford just throwing some exception on potentially critical parsing errors
     * and ignore some of them at all, like having draft values after * sign, for example.
     *
     * @section Format description
     * Parsing is based on digitToChar() implementation.
     * Unsolved digits are represented as a list of draft values without separators.
     * Solved digits are represented as a single value followed by '*' sign.
     * Digits are separated with spaces.
     * Examples:
     * * 123456789 - digits with full draft
     * * 3 - digit with one-value draft, not marked as solved
     * * 4* - solved digit with 4 as solution
     */
    template<digit_t MaxDigit>
    static constexpr Board<MaxDigit> parseBoard(const std::string_view str)
    {
        Board<MaxDigit> b(DraftState::Unmarked); // create non-standart unmarked board
        size_t digIdx{ 0 };
        // split returns range of ranges
        for (auto digstr : str | std::ranges::views::split(' ')) {
            auto& dig = b.getDigit(digIdx);
            std::ranges::for_each(digstr, getParseCharFunc(dig, digIdx));
            if (++digIdx == D * D) break;
        }
        if (digIdx < D * D) throw getErrorMsg("Too few digits", digIdx);

        return b;
    }
private:
    /** @brief Get error message with some details
     * @param detail Any details of the error, possibly string-like
     * @param place Some kind of location on where to find the cause of error
     * @return std::string with formated error message
     */
    static constexpr auto getErrorMsg(auto&& detail, auto&& place)
    {
        std::ostringstream s;
        s << "Parsing error at " << place << ": " << detail << "! Do a better job preparing the tests, would you?";
        return s.str();
    };

    /** @brief Helper function to parse one character and apply it to the digit
     * The digit could be a draft option or solution
     * @param dig Reference to a digit being currently parsed
     * @param digIdx Index of digit being parsed, mostly for error message verbosity
     */
    template<digit_t MaxDigit>
    static constexpr auto getParseCharFunc(Digit<MaxDigit>& dig, size_t digIdx)
    {
        return [&dig, digIdx](char c) {
            if (dig.isSolved()) throw getErrorMsg("Additional char for solved digit, space missing?", digIdx);
            if (c == '*') {
                if (!dig.trySolved())
                    throw getErrorMsg("Asterisk found, but no solution value provided", digIdx);
                return;
            }
            if (auto d{ charToDigit(c) }; d) {
                dig.mark(d.value());
                return;
            }
            throw getErrorMsg("Invalid char found", digIdx);
        };
    }

};
} // namespace sut

#endif // SU_TEST_TOOLS_HPP