#ifndef SU_TEST_PRINTING_HPP
#define SU_TEST_PRINTING_HPP

#include "defines.hpp"

#include <optional>
#include <string> 

using namespace su;
using std::views::filter;

namespace sut {

// Full set of possible digits
inline constexpr static auto digits = std::views::iota(digit_t{ 1 }, digit_t{ D + 1 });

// Full set of indexes for a square DxD board
inline constexpr static auto fullBoardIndexes = std::views::iota(size_t{ 0 }, size_t{ D * D });

// Some filters, perhaps will come useful more than once
inline constexpr static auto even = filter([](int i) { return i % 2 == 0; });
inline constexpr static auto odd = filter([](int i) { return i % 2 == 1; });

// Amount of digits in a string representation of a square board
static constexpr size_t BigD = (SizeD + size_t{ 1 }) * (SizeD * SizeD);

namespace impl {
static constexpr auto nums{ 10 }; // count of numeric digits
static constexpr auto chars{ 'Z' - 'A' + 1 }; // count of letters
/**
* @brief Returns a printable representation of a characted
* Starting from numbers, then uses lowercase latin latters, then uppercase latin latters
* Just in case of testing with D > 9
* @tparam char_type
* @param d
* @return constexpr char_type
*/
template <typename char_type = std::string::value_type>
static consteval char_type convertDigitToChar(const digit_t d)
{
    auto _d{ d };
    if (_d < nums) // 0..9
        return static_cast<char_type>(_d) + '0';
    _d -= nums;
    if (_d < chars) // 10..35
        return static_cast<char_type>(_d) + 'a';
    _d -= chars;
    if (_d < chars) // 36..61
        return static_cast<char_type>(_d) + 'A';
    _d -= chars;
    if (_d == 0) // 62
        return char_type{ '+' };
    _d--;
    if (_d == 0) // 63
        return char_type{ '-' };
    _d--;
    if (_d == 0) // 64
        return char_type{ '=' };
    return 0;
}

template<typename char_type = std::string::value_type>
static constexpr std::optional<digit_t> convertCharToDigit(const char_type c)
{
    auto _c{ c };
    if ('0' <= _c && _c <= '9')
        return { _c - '0' };
    if ('a' <= _c && _c <= 'z')
        return { _c - 'a' + 10 };
    if ('A' <= _c && _c <= 'Z')
        return { _c - 'A' + 10 + 26 };
    if (_c == '+')
        return 62;
    if (_c == '-')
        return 63;
    if (_c == '=')
        return 64;
    return {};
}

// funtion to get the digit-to-chat convertion array for printing
static consteval auto getDigitToCharMap()
{
    std::array<char, D + 1> result{ 0 };
    // std::ranges::copy(digits | std::views::transform(convertDigitToChar), result);
    for (const auto d : digits) {
        result[d] = convertDigitToChar(d);
    }
    return result;
}

// array to convert digit to char
static constexpr auto digitToCharMap = getDigitToCharMap();

// just a mini-test
static_assert(convertDigitToChar(0) == '0');
static_assert(convertDigitToChar(9) == '9');
static_assert(convertDigitToChar(10) == 'a');
static_assert(convertDigitToChar(64) == '=');

// mini-test
static_assert(convertCharToDigit(convertDigitToChar(4)).value() == 4);
static_assert(convertCharToDigit(convertDigitToChar(17)).value() == 17);
static_assert(convertCharToDigit(convertDigitToChar(27)).value() == 27);
static_assert(convertCharToDigit(convertDigitToChar(47)).value() == 47);
static_assert(convertCharToDigit(convertDigitToChar(59)).value() == 59);
static_assert(convertCharToDigit(convertDigitToChar(63)).value() == 63);

} // namespace impl

// func to convert digit to char
inline constexpr static char digitToChar(const digit_t d)
{
    return impl::digitToCharMap[d];
}

// view to convert digit to char
inline constexpr static auto d2c = std::views::transform([](const digit_t d) { return digitToChar(d); });

// func to convert char to digit
inline constexpr static std::optional<char> charToDigit(const char c)
{
    return impl::convertCharToDigit(c);
}

inline constexpr static auto c2d = std::views::transform([](const char c) { return charToDigit(c);});

/**
* @brief Generates the string for fully filled digit
*/
static constexpr auto fullDigitPrinted = []() {
    std::array<std::string::value_type, SizeD + 1> s{ 0 };
    std::generate(s.begin(), s.end() - 1, [n = 0]() mutable { return digitToChar(++n); });
    return s;
}();

/**
* @brief Generates the board fully filled with drafts
* Weird stuff
*/
static constexpr auto fullBoardPrinted = []() {
    std::array<std::string::value_type, BigD + 1> s{ 0 };
    std::generate(s.begin(), s.end() - 1, [n = 0]() mutable {
        n = (n + 1) % (D + 1);
        return n == 0 ? ' ' : digitToChar(n);
    });
    return s;
}();

}

#endif // SU_TEST_PRINTING_HPP
