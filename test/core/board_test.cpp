#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators_all.hpp>

#include "board.hpp"
#include "tools/test_tools.hpp"

using namespace su;

SCENARIO("Creating a board", "[core][board]")
{
    GIVEN("A new board")
    {
        Board b;
        THEN("Board is filled with drafts")
        {
            REQUIRE(sut::Formatter::toString(b) == sut::fullBoardPrinted.data());
        }
    }
}