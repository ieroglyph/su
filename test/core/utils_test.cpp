#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators_all.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include "utils.hpp"
#include "tools/test_tools.hpp"

#include <array>

using namespace su::utils;

namespace Catch {
template<std::ranges::range RangeT>
struct StringMaker<RangeT>
{
    static std::string convert(RangeT value)
    {
        std::ostringstream str;
        str << "{";
        size_t s(std::ranges::distance(value.begin(), value.end()));
        for (auto& v : value) {
            str << " " << static_cast<size_t>(v);
            if (--s) str << ',';
        }
        str << " }";
        return str.str();
    }
};
}

template<std::ranges::range RangeT>
struct EqualsRangeMatcher : Catch::Matchers::MatcherGenericBase
{
    EqualsRangeMatcher(RangeT const& range) :
        range{ range }
    {}

    bool match(auto&& other) const
    {
        return std::ranges::equal(range, other);
    }

    std::string describe() const override
    {
        return "Equals: " + Catch::rangeToString(range);
    }

private:
    RangeT range;
};

template<std::ranges::range RangeT>
auto EqualsRange(const RangeT& range) -> EqualsRangeMatcher<RangeT>
{
    return EqualsRangeMatcher<RangeT>{range};
};

SCENARIO("Testing stride filter", "[test][core][utils]")
{
    GIVEN("Some filter")
    {
        const auto testdata = std::array{ 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29 };

        auto filtr = get_stride_filter(9, 3);

        THEN("Correct filtering happens")
        {
            auto res = testdata | filtr;
            REQUIRE_THAT(res, EqualsRange(std::array{ 1, 2, 3, 10, 11, 12, 19, 20, 21, 28, 29 }));
        }
    }
}