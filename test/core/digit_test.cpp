#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators_all.hpp>

#include <algorithm>
#include <ranges>

#include "digit.hpp"
#include "tools/test_tools.hpp"

using namespace su;
using namespace sut;

SCENARIO("Marking digits", "[core][digit]")
{
    GIVEN("A new digit")
    {
        Digit d;

        THEN("Digit is all-maked")
        {
            for (const digit_t n : digits) {
                INFO("n == " << n);
                REQUIRE(d.isMarked(n));
            }
        }

        WHEN("unmark called")
        {
            d.unmark(digits | even);

            THEN("Those element becomes unmarked")
            {
                for (const digit_t n : digits | even) {
                    INFO("n == " << n);
                    REQUIRE(!d.isMarked(n));
                }
            }

            AND_THEN("Others stay marked")
            {
                for (const digit_t n : digits | odd) {
                    INFO("n == " << n);
                    REQUIRE(d.isMarked(n));
                }
            }
        }

        AND_WHEN("mark called")
        {
            for (const digit_t n : digits)
                d.mark(n);

            THEN("Elements becomes marked again")
            {
                for (const digit_t n : digits) {
                    INFO("n == " << n);
                    REQUIRE(d.isMarked(n));
                }
            }
        }
    }
}

SCENARIO("Solving digits", "[core][digit]")
{
    int sln = GENERATE_REF(from_range(digits.begin(), digits.end()));
    GIVEN("A new digit")
    {
        Digit d;

        WHEN("solve called")
        {
            d.solve(sln);

            THEN("Element becomes solved")
            {
                INFO("n == " << sln);
                REQUIRE(d.isSolved());
                REQUIRE(d.solution() == sln);
            }
        }

        WHEN("called checkSolved")
        {
            const auto checkResult = d.trySolved();
            THEN("check result is false")
            {
                REQUIRE(checkResult == false);
            }
            AND_THEN("solution is null")
            {
                auto solveResult = d.solution();
                REQUIRE(!solveResult.has_value());
            }
        }
    }

    GIVEN("A digit with only one marked value")
    {
        Digit d;
        d.mark(sln, true);

        WHEN("called checkSolved")
        {
            const auto checkResult = d.trySolved();

            THEN("check result is true")
            {
                REQUIRE(checkResult == true);
            }
            AND_THEN("result is correct")
            {
                const auto solveResult = d.solution();
                REQUIRE(solveResult.has_value());
                REQUIRE(*solveResult == sln);
            }
        }
    }
}
