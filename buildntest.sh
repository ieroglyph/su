#!/bin/bash

clear &&
rm -rf ./build &&
mkdir ./build &&
cd ./build &&
cmake .. &&
make -j &&
cd test &&
chmod +x ./SuTests &&
./SuTests