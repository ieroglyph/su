# Ruleset details

Conceptually, 
each ruleset is 
a collection of groups
within the same board
with same properties and rules.

Group is supposed to be 
a filter over the board
with a set of properties 
that the digits within the group
should meet.

A great example of a ruleset
would be a `Row`.
Each row could be represented as 
a group with the following properties:
 * Uniqueness: 
 every possible solution 
 for digits in the group 
 is allowed to be presented only once.
 * Completeness:
 every possible solution
 for digits in the group
 is obliged to be presented.
 