# Project structure

The project is organized in several modules:

 * Core: 
 basic simple description of the board
 with some digits.
 Each digit is described as a draft of 
 possible values for the digit.

 * Ruleset layers:
 ruleset layer are divided into several blocks:
   
   * save/load module: 
   some way to \[de\]serialize data;
   * check module: 
   some way to ckeck if the solution is correct;
   * solvers module:
   set of solvers appliable according 
   to the current set of the board and 
   the layers' additional information.

 * Guesser:
 basic recursive solver trying to find the solution 
 using the checkers only.

 * Tests?
