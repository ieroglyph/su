# Sudoku solver

## Description

> NB: 
I know that there are
existing solvers of that sort already.
That doesn't bother me at all.

This is about solving 
[sudoku](https://en.wikipedia.org/wiki/Sudoku) 
puzzles. 
The reason of starting this is rediculously simple: 
I used to like solving sudoku puzzles, 
but the more intriguing part of solving, 
for my brain as it is,
happens to be trying to formalize the approaches 
I am applying to the puzzles 
to solve every other digit.

Thus, this project is mainly 
to try to translate those approaches
to some finished algorithms.
And it to learn something about optimization and C++ 
in the process, 
if getting lucky.

Is it enought to use only the algorithms I can find myself
to solve the puzzle 
whithout falling to exhaustive search? 
I'd love to find that out!

Of course I'll not manage to keep myself from
finding the solution approaches 
in sources of some kind,
yet still the task of formalizing them
and translating into the code for electronic device
seems quite exciting. 
Yes, a weird brain I have. 

The other thing to consider is the priorities:

1. First of all, 
I'm using the things I want to learn. 

2. Second thing would be the possibility to use the things
in compile-time. 
It's not yet absolutely possibe to use all the stuff from std
with constexpr calls,
so that's the main reason this should go on the second place.

3. And the last and the least,
the optimization. 
There is always a possibility to reduce some complexity,
but that could become a gamebracker for the first two,
and in that case the optimization should be considered
not that important.

## Building

It should be enough to have some version of 
[GCC](https://gcc.gnu.org/), 
[CMake](https://github.com/Kitware/CMake) and 
[Catch22](https://github.com/catchorg/Catch2) 
installed to build the project. 
Or the provided 
[dockerfile](.devcontainer/dockerfile) 
should help.

Currently the only executable
is the tests application.
So, to build and run the tests
that is a convenient script prepared:
[buildntest.sh](./buildntest.sh).

## Project overview

The main idea is quite simple. 
First of all,
there are numerous of variations of Sudoku rules.
The general sudoku has quite a simple set of them,
and (usually) most of the other rulesets 
are extending the general rules in some way.
That suggest the main format 
of organizing the solver's code:
there gonna be layers.
The basic layer would contain the desk
and a set of approaches for solving 
the general sudoku.

The main thing to emphasized here
must be the fact
that only the board itself is 
something that forms the basic layer: 
only a N by N field of digits; 
but the shapes of regions are not specified
and are only added by other layers.
Isn't that cool?

The other layers are going to be
the extensions of some kind.
"Killer sudoku" is going to happen to be
the first such a layers,
just based on my personal preferences
and my view of what is possible for me
to implement at the moment.

Look for more details in the 
[docs](./docs)
directory. 
(How am I kidding? 
Noone is going to read any of this)

## Licensing info

The 
[MIT license](https://mit-license.org/) 
is good enough.
