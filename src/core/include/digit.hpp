#ifndef CORE_DIGIT_HPP
#define CORE_DIGIT_HPP

#include "defines.hpp"

#include <algorithm>
#include <array>
#include <concepts>
#include <numeric>
#include <optional>
#include <ranges>
#include <utility>
#include <stdexcept>
#include <type_traits>

using std::array;
using std::optional;
using std::views::filter;

namespace su {

/** @brief Describes intention to do something with drafts.
 */
enum class DraftState
{
    Marked = 1, //< Set draft as marked
    Unmarked = 0, //< Set draft as unmarked
};

/// Concept for types convertible to digit_t
template <class T>
concept DigitT = std::convertible_to<T, digit_t>;

/// Concept for types representing ranges of digit_t
template <class T>
concept RangeOfDigitT = std::ranges::range<T> && requires(T t)
{
    { *t.begin() } -> DigitT;
};

/// Concept for anything that is convertible to digit_t or a range of those
template <class T>
concept RangeOfOrDigitT = DigitT<T> || RangeOfDigitT<T>;

/**
* @brief Description for a digit with basic digit-based algorithms
* Available digits are from 1 till MaxDigit: there is no 0 in sudoku game!
* @tparam MaxDigit Maximum number of digits
*/
template<digit_t MaxDigit = D>
class Digit
{
public:
    /**
     * @brief Constructor taking DraftState
     * In most cases, algorithms should expect to have marked by default digits.
     * Creating unmarked one could be more convenient reading from stored state.
     */
    explicit constexpr Digit(DraftState draftState = DraftState::Marked)
    {
        resetDraft(draftState);
    };

    /**
     * @brief Checks if the digit is marked
     */
    inline constexpr bool isMarked(const digit_t n) const
    {
        return (m_draft[n - 1] == n);
    };

    /**
     * @brief Marks specified digit as possible draft
     * @param ns Value or range of values to mark
     * @param reset True if other are expected to become unmarked
     */
    inline constexpr void mark(const RangeOfOrDigitT auto& ns, bool reset = false)
    {
        if (reset) {
            resetDraft(DraftState::Unmarked);
        }
        mark_impl(ns, 1);
    }

    /**
     * @brief Unmarks specified digit as not possible draft
     * @param ns Value or arnge of values to unmark
     * @param reset True if other are expected to become marked
     */
    inline constexpr void unmark(const RangeOfOrDigitT auto& ns, bool reset = false)
    {
        if (reset) {
            resetDraft(DraftState::Marked);
        }
        mark_impl(ns, 0);
    }

    /**
     * @brief Check whether the digit is solved
     * @return True is solution is found, false otherwise
     */
    inline constexpr bool isSolved() const
    {
        return m_solution != 0;
    }

    /**
     * @brief Returns solution of the digit is presented
     * @return digit_t if solution is found, std::nullopt otherwise
     */
    inline constexpr optional<digit_t> solution() const
    {
        return m_solution != 0 ? std::make_optional(m_solution) : std::nullopt;
    }

    /**
     * @brief Sets solution of the digit
     * The draft becomes completely unmarked, so no possible guesses available
     * @param n Solution to be set
     */
    inline constexpr void solve(const digit_t n)
    {
        resetDraft(DraftState::Unmarked);
        m_solution = n;
    }

    /**
     * @brief Get range of all marked values
     * @return Range of marked values
     */
    inline constexpr auto getMarked() const
    {
        return m_draft | filterMarked;
    }

    /**
     * @brief Check if solution is available
     * If only one draft value is marked as possible,
     * set it as the correct solution for the digit.
     * @return True if already solved or was solved during the check
     */
    inline constexpr bool trySolved()
    {
        if (isSolved()) {
            return true;
        }
        auto boop = getMarked();
        if (boop.front() != boop.back()) {
            return false;
        }
        solve(boop.front());
        return true;
    }

    /**
    * @brief Resets the draft
    * @param marked Selects if we want to set as marked or non-marked
    */
    inline constexpr void resetDraft(DraftState marked)
    {
        switch (marked) {
        case DraftState::Marked:
            std::iota(m_draft.begin(), m_draft.end(), 1);
            break;
        case DraftState::Unmarked:
            std::fill(m_draft.begin(), m_draft.end(), 0);
            break;
        }
    }

private:
    /** Draft — possible solutions of MaxDigit digits
    * represented as array of MaxDigit elements of digit_t
    * where draft[N]==N+1 if N is marked or
    * draft[N]==0 if N is not marked or
    * draft[N]==N+1 if N is marked
    */
    array<digit_t, MaxDigit> m_draft;

    /// Solution, 0 if not found yet, 1..MaxDigit otherwise
    digit_t m_solution{ 0 };

    /// Filter to search for marked values in all-draft range
    static inline constexpr auto filterMarked = filter([](auto i) { return i != 0; });

    /**
     * @brief Implementation for marking func for single digit
     * See range-specialized mark_impl for details
     * @param n Digit value
     * @param mul 1 if needs to mark, 0 otherwise
     */
    inline constexpr void mark_impl(DigitT auto n, int mul)
    {
        mark_impl(std::ranges::single_view{ n }, mul);
    }

    /** @brief Implementation for marking of a range of digits
     * @param n Digit value
     * @param mul 1 if needs to mark, 0 otherwise
     */
    inline constexpr void mark_impl(RangeOfDigitT auto ns, int mul)
    {
        for (const auto& n : ns) {
            if (n > 0)
                m_draft[n - 1] = n * mul;
        }
    }
};

}

#endif // CORE_DIGIT_HPP