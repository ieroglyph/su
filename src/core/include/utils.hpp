#ifndef CORE_UTILS_HPP
#define CORE_UTILS_HPP

#include <ranges>

namespace su::utils {

namespace impl {
/** Returns lambda the returns true for the first n times, then false for step - n times
 */
static inline constexpr auto get_stride_func_n(size_t step, size_t n)
{
    return[m = 0, step, n](auto&&) mutable { m %= step; m++; return m <= n; };
}
// sanity check...
static_assert(0 % 9 == 0);
static_assert(9 % 9 == 0);
}


static inline constexpr auto get_stride_filter(size_t step, size_t n = 1)
{
    using std::views::filter;
    return filter(impl::get_stride_func_n(step, n));
}

}

#endif // CORE_UTILS_HPP