#ifndef CORE_BOARD_HPP
#define CORE_BOARD_HPP

#include "defines.hpp"
#include "digit.hpp"
#include <memory>
#include <ranges>
#include <cassert>

namespace su {

/**
 * @brief Board of digits
 * Board does not know anything about it's shape,
 * or anything else but the overall amount of the digits.
 */
template <digit_t MaxDigit = D, size_t NumberOfDigits = (D* D)>
class Board
{
public:
    /**
     * @brief Alias for type of the Digit
     */
    using digit_type = su::Digit<MaxDigit>;

    /**
     * @brief Creates board with digits of specified marked state
     */
    explicit constexpr Board(DraftState draftState = DraftState::Marked)
    {
        for (auto& d : m_digits) {
            d.resetDraft(draftState);
        }
    };

    /**
     * @brief Get the Digit object by index
     *
     * @param idx Index of the digit
     * @return constexpr digit_type&
     */
    constexpr digit_type& getDigit(size_t idx)
    {
        assert(idx < NumberOfDigits);
        return m_digits[idx];
    }

    /**
     * @brief Get all the digits (as a range, perhaps)
     *
     * @return constexpr auto&
     */
    constexpr auto& allDigits()
    {
        return m_digits;
    }

private:
    /**
     * @brief Array of digits
     */
    std::array<digit_type, NumberOfDigits> m_digits;
};

} // namespace su

#endif // CORE_BOARD_HPP