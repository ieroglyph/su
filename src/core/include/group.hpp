#ifndef CORE_GROUP_HPP
#define CORE_GROUP_HPP

#include "defines.hpp"
#include <concepts>

namespace su {

// Concept for group consisting of only the unique solutions for each digit
template<class T>
concept unique_group = requires
{ T::is_unique == true; };

// Concept for group containing full set of solutions
template<class T>
concept full_group = unique_group<T> && requires
{ T::const_size == size_t{ D }; };

} // namespace su


#endif // CORE_GROUP_HPP