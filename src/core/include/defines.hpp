#ifndef CORE_DEFINES_HPP
#define CORE_DEFINES_HPP

#include <cstddef>
#include <type_traits>

namespace su {

// Type to store digits in memory
using digit_t = unsigned char;

// Nessessary checks for the digit_t type
static_assert(std::is_integral_v<digit_t>);
static_assert(std::is_unsigned_v<digit_t>);

// Maximum value for the digit on the board,
// also defines the size of the square board to be D by D
static constexpr digit_t D = 9;

// Just a size_t version of D (and also printable with cout)
static constexpr size_t SizeD = size_t{ D };
}

#endif // CORE_DEFINES_HPP