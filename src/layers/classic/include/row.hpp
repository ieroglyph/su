#ifndef LAYER_CLASSIC_ROW_HPP
#define LAYER_CLASSIC_ROW_HPP

#include "board.hpp"
#include "utils.hpp"

#include <utility>

namespace su::classic {

/** Row - only for square boards
 * Represents a way to access all digits of a row on a board
 * given the board and a row number.
 */
template<digit_t MaxDigit = D>
struct Row
{
    using board_t = Board<MaxDigit>;
    static constexpr bool is_unique{ true };
    static constexpr size_t const_size{ MaxDigit };

    inline constexpr Row(board_t& board, size_t idx) : m_board{ board }, m_idx{ idx }
    {};

    inline auto digits()
    {
        using std::views::filter;
        using std::ranges::views::drop;
        using std::ranges::views::take;
        const auto toDrop{ m_idx * D };
        return m_board.allDigits() | drop(toDrop) | take(D);
    }

private:
    board_t& m_board;
    size_t m_idx;
};

}

#endif // LAYER_CLASSIC_ROW_HPP