#ifndef LAYER_CLASSIC_COL_HPP
#define LAYER_CLASSIC_COL_HPP

#include "board.hpp"
#include "utils.hpp"

#include <utility>

namespace su::classic {

/** Col - only for square boards
 * Represents a way to access all digits of a column on a board
 * given the board and a column number.
 */
template<digit_t MaxDigit = D>
struct Col
{
    using board_t = Board<MaxDigit>;
    static constexpr bool is_unique{ true };
    static constexpr size_t const_size{ MaxDigit };

    inline constexpr Col(board_t& board, size_t idx) : m_board{ board }, m_idx{ idx }
    {};

    inline auto digits()
    {
        using std::views::filter;
        using std::ranges::views::drop;
        // waiting for ranges::stride_view...
        auto stride_filter{ su::utils::get_stride_filter(MaxDigit) };
        const auto toDrop = m_idx;
        return m_board.allDigits() | drop(toDrop) | stride_filter;
    }

private:
    board_t& m_board;
    size_t m_idx;
};

}

#endif // LAYER_CLASSIC_COL_HPP