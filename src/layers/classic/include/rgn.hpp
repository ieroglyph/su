#ifndef LAYER_CLASSIC_RGN_HPP
#define LAYER_CLASSIC_RGN_HPP

#include "board.hpp"
#include "utils.hpp"

#include <cmath>
#include <utility>

namespace su::classic {

/** Rgn - only for square boards
 * Represents a way to access all digits of a region on a board
 * given the board and a region number,
 * labeled as follows:
 *  0 | 1 | 2
 * ---+---+---
 *  3 | 4 | 5
 * ---+---+---
 *  6 | 7 | 8
 */
template<digit_t MaxDigit = D>
struct Rgn
{
    using board_t = Board<MaxDigit>;
    static constexpr bool is_unique{ true };
    static constexpr size_t const_size{ MaxDigit };

    inline constexpr Rgn(board_t& board, size_t idx) : m_board{ board }, m_idx{ idx }
    {};

    inline auto digits()
    {
        using std::views::filter;
        using std::ranges::views::drop;
        using std::ranges::views::take;

        const auto rgnWidth = size_t(std::sqrt(MaxDigit));

        const auto rowsToDrop{ m_idx / rgnWidth * rgnWidth }; // once
        const auto colsToDrop{ m_idx % rgnWidth * rgnWidth }; // search row
        auto stride_filter{ su::utils::get_stride_filter(MaxDigit, rgnWidth) };

        return m_board.allDigits() | drop(rowsToDrop * MaxDigit + colsToDrop) | stride_filter | take(MaxDigit);
    }

private:
    board_t& m_board;
    size_t m_idx;
};

}

#endif // LAYER_CLASSIC_RGN_HPP