#include "col.hpp" // self

#include "group.hpp"

namespace su::classic {

// some checks
static_assert(unique_group<Col<>>);
static_assert(full_group<Col<>>);

}