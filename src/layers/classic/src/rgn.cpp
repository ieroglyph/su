#include "rgn.hpp" // self

#include "group.hpp"

namespace su::classic {

// some checks
static_assert(unique_group<Rgn<>>);
static_assert(full_group<Rgn<>>);

}