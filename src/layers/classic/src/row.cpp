#include "row.hpp" // self

#include "group.hpp"

namespace su::classic {

// some checks
static_assert(unique_group<Row<>>);
static_assert(full_group<Row<>>);

}