#include <iostream>
#include <array>
#include <vector>
#include <list>
#include <algorithm>
#include <numeric>
#include <string>
#include <sstream>
#include <tuple>

using namespace std;

namespace {
const size_t D = 9;
using nut = unsigned char;
}

class House;

class Digit
{
public:
    Digit()
    {
        iota(draft.begin(), draft.end(), 1);
    }

    bool solvable(nut *solution = nullptr) const
    {
        auto singleMark = false;
        nut solutionTmp = 0;
        for (const auto& d: draft) {
            if (singleMark && d!=0)
                return false;
            if (d != 0) {
                solutionTmp = d;
                singleMark = true;
            }
        }
        if (singleMark && solution != nullptr)
            *solution = solutionTmp;
        return singleMark;
    }

    nut solution() const
    {
        nut solution = 0;
        solvable(&solution);
        return solution;
    }

    void solve(nut n)
    {
        if (solved) return;
        fill(draft.begin(), draft.end(), 0);
        draft[4] = n;
        solved = true;
        houses.clear();
    }

    void mark(nut n, bool v = true)
    {
        draft[n-1] = v ? n : 0;
    }

    void unmark(nut n)
    {
        mark(n, false);
    }

    bool solved {false};
    array<nut, D> draft;
    list<House*> houses;
};

// Group of D digits, where no digit can be repeated twice
// Usually, it's D rows, D columns and D cells.
class House
{
public:
    House()
    {

    }
    void solve(Digit* digit, nut s)
    {
        auto dd = digits.end();
        for (auto d = digits.begin(); d != digits.end(); d++) {
            if (*d == digit) dd = d;
            else (*d)->unmark(s);
        }
        if (dd != digits.end())
            digits.erase(dd);
    }
    list<Digit*> digits;
};

class Board
{
public:
    // construct digits and houses, set their relations
    Board()
    {
        fill(digits.begin(), digits.end(), Digit());
        const auto connect = [this](size_t h, size_t d){
            houses[h].digits.push_back(&digits[d]);
            digits[d].houses.push_back(&houses[h]);
        };
        for (size_t x = 0; x < D; x++) {
            for (size_t y = 0; y < D; y++) {
                connect(x, y*D+x); // cols
                connect(D+x, x*D+y); // rows
                connect(2*D+x, (x/3*3 + y%3)*D+(y/3 + x%3*3));
            }
        }
    }
    // print prety and verbose state of the board
    void print()
    {
        cout << "╔═══════╦═══════╦═══════╦═══════╦═══════╦═══════╦═══════╦═══════╦═══════╗" << endl;
        int space = 0;
        int dblv = 0;
        int dblh = 0;
        for (size_t y = 0; y < D; y++) {
            for (size_t z1 = 0; z1 < 3; z1++) {
                cout << "║";
                for (size_t x = 0; x < D; x++) {
                    cout << " ";
                    for (size_t z2 = 0; z2 < 3; z2++) {
                        const auto v = static_cast<int>(digits[y*D+x].draft[z1*3+z2]);
                        if (v == 0) cout << " "; else cout << v;
                        if (space++ < 2) cout << " ";
                        else {
                            if (dblv++%3 != 2) cout << " │";
                            else cout << " ║";
                            space = 0;
                        }
                    }
                }
                cout << endl;
            }
            if (dblh++%3!=2) {
                cout << "╠───────┼───────┼───────╬───────┼───────┼───────╬───────┼───────┼───────╣" << endl;
            } else if (y != D-1) {
                cout << "╠═══════╬═══════╬═══════╬═══════╬═══════╬═══════╬═══════╬═══════╬═══════╣" << endl;
            }
        }
        cout << "╚═══════╩═══════╩═══════╩═══════╩═══════╩═══════╩═══════╩═══════╩═══════╝" << endl;
    }
    // set specified digit as solved with value s
    void solve(Digit *d, nut s)
    {
        for (auto house: d->houses) {
            house->solve(d, s);
        }
        d->solve(s);
    }
    // set specified ditig by it's coords as solved with value s
    void solve(size_t x, size_t y, nut s)
    {
        solve(&digits[D*y + x],s);
    }
    // read from a string, ignoring spaces and line breaks, with . as unsolved digit
    bool load(string s)
    {
        const auto isNum = [](auto c){
            auto lbound = '1'-1;
            auto rbound = '9'+1;
            return ((c > lbound) && (c < rbound));
        };
        if (s.size() < D*D) return false; // s too short
        size_t y = 0;
        size_t x = 0;
        for (auto& c: s) {
            if (isNum(c)) {
                solve(x,y,c-'0');
                x++;
            } else if (c == '.') {
                x++;
            }
            if (x == D) {
                x = 0;
                y++;
            }
        }
        return true;
    }
    // save to a slightly formatted string
    string save()
    {
        ostringstream s;
        for (size_t y = 0; y < D; y++) {
            for (size_t x = 0; x < D; x++) {
                nut sol;
                digits[y*D + x].solvable(&sol) ? s << static_cast<char>(sol+'0') : s << '.';
                s << " ";
            }
            s << endl;
        }
        return s.str();
    }
    // collect every digit that has only one marked value
    // returns true if at least one digit was solved
    bool collectSolvables()
    {
        bool smthngChanged = false;

        for (auto &d: digits) {
            nut sol;
            if (!d.solved && d.solvable(&sol)) {
                solve(&d,sol);
                smthngChanged = true;
            }
        }

        return smthngChanged;
    }
    // search for markings that appear only once per house
    // returns true if at least one digit was solved with that
    bool collectSingulars()
    {
        bool smthngChanged = false;
        array<tuple<bool,Digit*>, D> cur;
        for (auto& house: houses) {
            fill(cur.begin(), cur.end(), make_tuple(false, nullptr));
            for (auto& digit: house.digits) {
                for (auto& d: digit->draft) {
                    if (d == 0) continue;
                    if (auto [visited, _] = cur[d-1]; visited == true) {
                        cur[d-1] = make_tuple(true, digit);
                    } else {
                        cur[d-1] = make_tuple(true, nullptr);
                    }
                }
            }
            size_t i = 1;
            for (auto [visited, digit]: cur) {
                if (visited && digit != nullptr) {
                    solve(digit, i);
                    smthngChanged = true;
                }
                i++;
            }
        }
        return smthngChanged;
    }
    // is the board solved?
    bool solved()
    {
        for (const auto& house: houses) {
            if (house.digits.size() > 0)
                return false;
        }
        return true;
    }
private:
    array<Digit, D*D> digits;
    array<House, D*3> houses;
};

int main()
{
    Board b;

    b.load("9 . . . . . . . 1"
           ". . 7 8 3 1 6 4 9"
           "6 1 . 5 4 . 8 . ."
           ". . . 1 . . . . 6"
           "7 4 5 . 9 6 2 . ."
           ". . 6 . . 4 7 5 ."
           "3 7 . 4 . . 9 . 2"
           "4 . . . 6 . . 8 5"
           "5 . 1 . . 8 . . .");
    int reps = 0;
    cout << b.save();
    bool changes = false;
    while((!b.solved()) && reps < D*D)
    {
        bool c1 = b.collectSolvables();
        bool c2 = b.collectSingulars();
        changes = c1 || c2;
        reps++;
        cout << c1 << " " << c2 << endl;
        cout << b.save();
    }
    return 0;
}
