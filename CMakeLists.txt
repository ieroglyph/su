cmake_minimum_required(VERSION 3.23.0)
project(Su VERSION 0.1.0 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 23)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
# still need to specify -std manually: cmake does not translate 23 to 2b
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++2b -fconcepts-diagnostics-depth=2")

add_subdirectory(src)

add_subdirectory(test)

